<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Artikel extends Model
{
    protected $table = 'blogs';
    protected $primarykey = 'id_blog';
}
