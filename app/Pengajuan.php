<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pengajuan extends Model
{
    protected $table = 'penghargaans';
    protected $primarykey = 'id_penghargaan';
}
